from Tkinter import *

fenetre = Tk()
dimension = 5

fenetre.grid()

grille = [[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5]]
case_visible = [[0, 0, 0, 0, 0],[0, 0, 0, 0, 0],[0, 0, 1, 1, 2],[0, 0, 1, 1, 2],[0, 0, 1, 1, 2]]

def motion(event):
    x, y = event.x, event.y
    print('{}, {}'.format(x, y))
            

for i in range(dimension):
    for j in range (dimension):
        ligne = Button(fenetre, height=1, width=1, command=lambda y=i, x=j: test(y, x)).grid(row=i, column=j)
        
# fenetre.bind('<Button 1>', motion)

fenetre.mainloop()