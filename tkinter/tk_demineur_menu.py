from tkinter import *
import tk_dem_global

def creer_menu(): 
    tk_dem_global.difficulte = 0
    global fenetre
    fenetre = Tk()
    fenetre.withdraw()
    fenetre.update_idletasks()
    l = 750
    h = 850
    x = (fenetre.winfo_screenwidth() - l) / 2
    y = (fenetre.winfo_screenheight() - h) / 2
    fenetre.geometry('%dx%d+%d+%d' % (l, h, x, y))
    fenetre.deiconify()
    
    def afficher_score():
        if tk_dem_global.page == 0:
            afficher_menu()
        if tk_dem_global.page == 1:
            titre = 'High Score 10x10'
        if tk_dem_global.page == 2:
            titre = 'High Score 15x15'
        if tk_dem_global.page == 3:
            titre = 'HIgh score 20x20'
        txt_score = Label(fenetre, text=titre, font='ARIAL 35', pady=30)
        txt_score.pack(side=TOP)
        fleche_gauche = Button(fenetre, text='<-', font='ARIAL 45', command=tourner_page_vers_gauche)
        fleche_gauche.pack(side=LEFT)
        if not tk_dem_global.page == 3:
            fleche_droite = Button(fenetre, text='->', font='ARIAL 45', command=tourner_page_vers_droite)
            fleche_droite.pack(side=RIGHT)
        if tk_dem_global.page == 3:
            fleche_droite = Button(fenetre, text='   ', font='ARIAL 45', relief=FLAT)
            fleche_droite.pack(side=RIGHT)
        bouton_quit = Button(fenetre, command=quitter, text='Quitter', font='ARIAL 30', padx=15, pady=10)
        bouton_quit.pack(side=BOTTOM, pady=10)
        fichier = open('tkinter/score_fichier', 'r')
        liste = []
        for line in fichier:
            liste.append(line)
        for i in range(10):
            txt_score = Label(fenetre, text= str(i+1) + ' : ' + liste[i+(11*tk_dem_global.page)-10].strip() + ' - ' + liste[i+(11*(tk_dem_global.page+3))-10], font='ARIAL 18')
            txt_score.pack(fill=X)
        fichier.close()
    
    def difficulte_20x20():
        tk_dem_global.difficulte = 3
        fenetre.destroy()
    
    def difficulte_15x15():
        tk_dem_global.difficulte = 2
        fenetre.destroy()
    
    def difficulte_10x10():
        tk_dem_global.difficulte = 1
        fenetre.destroy()
    
    def quitter():
        tk_dem_global.continuer_de_jouer = 0
        fenetre.destroy()
    
    def tourner_page_vers_droite():
        clear_fenetre()
        tk_dem_global.page += 1
        afficher_score()
    
    def tourner_page_vers_gauche():
        clear_fenetre()
        tk_dem_global.page -= 1
        afficher_score()
    
    def clear_fenetre():
        for widget in fenetre.winfo_children():
            widget.pack_forget()
    
    def afficher_menu():
        bouton_5x5 = Button(fenetre, command=difficulte_10x10, pady=20, padx=50, text="10x10", font='ARIAL 40')
        bouton_10x10 = Button(fenetre, command=difficulte_15x15, pady=20, padx=50, text="15x15", font='ARIAL 40')
        bouton_15x15 = Button(fenetre, command=difficulte_20x20, pady=20, padx=50, text="20x20", font='ARIAL 40')
        bouton_score = Button(fenetre, command=tourner_page_vers_droite, pady=10, padx=40, font='ARIAL 40', text='Score')
        bouton_quit = Button(fenetre, pady=20, padx=30, text="Quitter", font='ARIAL 30', command=quitter)
        titre = Message(fenetre, font='ARIAL 90 bold', text="DEMINEUR", aspect=500, anchor='n', pady=75)
        titre.pack()
        bouton_quit.pack(side=BOTTOM)
        bouton_5x5.pack(side=TOP)
        bouton_10x10.pack()
        bouton_15x15.pack()
        bouton_score.pack(pady=30)
    afficher_menu()
 
    fenetre.mainloop()

