from tkinter import *
from tk_demineurseed import *
from tk_demineur_case_visible import * 
import tk_dem_global
import time
from threading import Thread
from math import *
from sys  import exit

def affichage_fenetre():
    global x
    global y
    global terrain
    tk_dem_global.list_btn = []
    for i in range(tk_dem_global.dimension):
        ligne_btn = [None] * tk_dem_global.dimension
        tk_dem_global.list_btn.append(ligne_btn)  
    tk_dem_global.case_decouverte = []
    for colonne in range(tk_dem_global.dimension):
        ligne = [0] * tk_dem_global.dimension
        tk_dem_global.case_decouverte.append(ligne)
    terrain = Tk()
    terrain.withdraw()
    Grid.rowconfigure(terrain, 0, weight=1)
    Grid.columnconfigure(terrain, 0, weight=1)
    terrain.grid()
    terrain.update_idletasks()
    l = tk_dem_global.dimension * 31
    h = tk_dem_global.dimension * 31 + 62
    x = (terrain.winfo_screenwidth() - l) / 2
    y = (terrain.winfo_screenheight() - h) / 2
    terrain.geometry('%dx%d+%d+%d' % (l, h, x, y))
    terrain.deiconify()
    if tk_dem_global.continuer_de_jouer == 1:
        tk_dem_global.partie_en_cours = 1
        Grid.rowconfigure(terrain, 0, weight=1)
        txt_temps = Label(terrain, pady=5, text='Temps = 0', font="ARIAL 15")
        txt_temps.grid(row =0, columnspan=5)
        class score(Thread):
            def __init__(self):
                Thread.__init__(self)
            def run(self):
                montemps = time.time()
                while tk_dem_global.partie_en_cours == 1:
                    temps1 = time.time()-montemps
                    temps2 = ceil(temps1)
                    tk_dem_global.timer = temps2
                    txt_temps.config(text='Temps = ' + str(temps2))
        thread_score = score()
        thread_score.start()
        for i in range(tk_dem_global.dimension):
            for j in range(tk_dem_global.dimension):
                Grid.rowconfigure(terrain, i+1, weight=1)    #https://stackoverflow.com/questions/7591294/how-to-create-a-self-resizing-grid-of-buttons-in-tkinter
                Grid.columnconfigure(terrain, j, weight=1)
                tk_dem_global.list_btn[i][j] = Button(terrain, relief=RAISED, text='', font='ARIAL 12 bold', height=1, width=1, command=lambda y=i, x=j: click_case(y, x))
                tk_dem_global.list_btn[i][j].grid(row=i+1, column=j, sticky=N+S+E+W)
        terrain.mainloop()
        thread_score.join()
    else:
        terrain.destroy()
        
def click_case(y, x):
    tk_dem_global.case_decouverte[y][x] = 1
    case_visible_def()
    affichage()

def affichage():
    for i in range(tk_dem_global.dimension):
        for j in range(tk_dem_global.dimension):
            if tk_dem_global.case_decouverte[i][j] == 1:
                if tk_dem_global.grille[i][j] == 0:
                    tk_dem_global.list_btn[i][j].config(text='', font='ARIAL 12 bold', relief=FLAT)
                if tk_dem_global.grille[i][j] == 1:
                    tk_dem_global.list_btn[i][j].config(text=tk_dem_global.grille[i][j], font='ARIAL 12 bold', relief=FLAT, fg='blue', activeforeground='blue')
                if tk_dem_global.grille[i][j] == 2:
                    tk_dem_global.list_btn[i][j].config(text=tk_dem_global.grille[i][j], font='ARIAL 12 bold', relief=FLAT, fg='green', activeforeground='green')
                if tk_dem_global.grille[i][j] == 3:
                    tk_dem_global.list_btn[i][j].config(text=tk_dem_global.grille[i][j], font='ARIAL 12 bold', relief=FLAT, fg='red', activeforeground='red')
                if tk_dem_global.grille[i][j] == 4:
                    tk_dem_global.list_btn[i][j].config(text=tk_dem_global.grille[i][j], font='ARIAL 12 bold', relief=FLAT, fg='gray12', activeforeground='gray12')
                if tk_dem_global.grille[i][j] == 5:
                    tk_dem_global.list_btn[i][j].config(text=tk_dem_global.grille[i][j], font='ARIAL 12 bold', relief=FLAT, fg='orange', activeforeground='orange')
                if tk_dem_global.grille[i][j] == 6:
                    tk_dem_global.list_btn[i][j].config(text=tk_dem_global.grille[i][j], font='ARIAL 12 bold', relief=FLAT, fg='purple', activeforeground='purple')
                if tk_dem_global.grille[i][j] == 7:
                    tk_dem_global.list_btn[i][j].config(text=tk_dem_global.grille[i][j], font='ARIAL 12 bold', relief=FLAT, fg='maroon 2', activeforeground='maroon2')
                if tk_dem_global.grille[i][j] == 8:
                    tk_dem_global.list_btn[i][j].config(text=tk_dem_global.grille[i][j], font='ARIAL 12 bold', relief=FLAT, fg='deep sky blue', activeforeground='deep sky blue')
                if tk_dem_global.grille[i][j] > 8:
                    defaite()
                    break
            elif tk_dem_global.case_decouverte[i][j] == 0:
                tk_dem_global.list_btn[i][j].config(text='') 
            victoire_sans_drapeau = 0
            for m in range(tk_dem_global.dimension):
                for n in range(tk_dem_global.dimension):
                    if tk_dem_global.case_decouverte[m][n] == 0:
                        victoire_sans_drapeau += 1
            if victoire_sans_drapeau == tk_dem_global.nb_mine:
                victoire()

def defaite():
    global terrain
    tk_dem_global.partie_en_cours = 0
    def destroy_defaite():
        root_defaite.destroy()
    terrain.destroy()
    root_defaite = Tk()
    root_defaite.withdraw()
    root_defaite.update_idletasks()
    l = 500
    h = 150
    x = (root_defaite.winfo_screenwidth() - l) / 2
    y = (root_defaite.winfo_screenheight() - h) / 2
    root_defaite.geometry('%dx%d+%d+%d' % (l, h, x, y))
    root_defaite.deiconify()
    Titre = Label(root_defaite, font='ARIAL 60', text='PERDU', pady=20, padx=20)
    Titre.pack()
    root_defaite.after(1500, destroy_defaite)  

def victoire():
    global terrain
    tk_dem_global.partie_en_cours = 0
    
    def destroy_victoire():
        root_victoire.destroy()
    
    terrain.destroy()
    root_victoire = Tk()
    root_victoire.withdraw()
    root_victoire.update_idletasks()
    l = 500
    h = 150
    x = (root_victoire.winfo_screenwidth() - l) / 2
    y = (root_victoire.winfo_screenheight() - h) / 2
    root_victoire.geometry('%dx%d+%d+%d' % (l, h, x, y))
    root_victoire.deiconify()
    Titre = Label(root_victoire, font='ARIAL 60', text='BRAVO', pady=20, padx=20)
    Titre.pack()
    root_victoire.after(1500, destroy_victoire)  
    
    def recup_text():
        pseudo = ent.get()
        fichier = open("tkinter/score_fichier", "r")
        liste_score = []
        for line in fichier:
            liste_score.append(line)
        fichier.close()
        fichier = open('tkinter/score_fichier', 'w')
        classement = 10
        for i in range(10):
            if tk_dem_global.timer < int(liste_score[i+(11*(tk_dem_global.difficulte+3))-10]):
                classement -=1
        if not classement == 11:
            liste_score.insert(classement+(11*(tk_dem_global.difficulte+3))-10, str(tk_dem_global.timer) + '\n')
            liste_score.pop(11*(tk_dem_global.difficulte+3))
            liste_score.insert(classement+(11*tk_dem_global.difficulte)-10, pseudo + '\n')
            liste_score.pop(11*tk_dem_global.difficulte)
        fichier.writelines(liste_score)
        fichier.close()
        root.destroy()
    
    root = Tk()
    txt = Label(root, text='Quel est votre nom ?')
    txt.pack(side=TOP)
    ent = Entry(root)
    ent.insert(0, '')
    ent.pack(side=TOP, fill=X)
    bouton1 = Button(root, text='Confirmer', command=recup_text)
    bouton1.pack(side=BOTTOM)
    root.mainloop()

while tk_dem_global.continuer_de_jouer == 1:
    creer_grille()
    affichage_fenetre()
    



